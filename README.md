# Origin

According to the original author https://twitter.com/biochem_fan/status/1228073392265465856 : 

* The code works only for amyloid filaments, whose chain is almost flat on the x-y plane. This is just a proof-of-concept done in a few hours. Please use at your own risk.

* Provide only one layer of your filament. If the layer consists of multiple chains (e.g. dimer), renumber residue IDs so that they don't overlap.

# Installing

Just put the .inx and the .py files in `~/.config/inkscape/extensions/` folder on unix, or `%appdata%\inkscape\extensions\` on windows, then "opening" files with inkscape should work, provided the file is supported by the script

If you use Inkscape 1.0-beta (or any version more recent than 0.92.4), clone the "dev" branch, there has been changes in how extensions work
